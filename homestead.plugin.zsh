# Provide easy access to your homestead environment
# allowing you to access it from anywhere in your system.
# There are also some extra googies thrown in for good
# measure. Enjoy!

function hs() {(
    # Determine if md5sum is installed
    if hash md5 2>/dev/null; then
        MD5='md5'
    elif hash md5sum 2>/dev/null; then
        MD5='md5sum'
    else
        MD5=''
    fi

    # Default location for homestead config
    if [ -z "$HOMESTEAD_CFG" ]; then
        HOMESTEAD_CFG=~/Homestead/Homestead.yaml
    fi

    # Default editor to nano
    if [ -z "$EDITOR" ]; then
        EDITOR='nano'
    fi

    case "$*" in

        # Default to SSH
        "")
            CMD='ssh'
            ;;

        # Edit Homestead config file
        "config" | "cfg")
            # No md5sum tool found, just edit config
            if [ -z $MD5 ]; then
                $EDITOR "$HOMESTEAD_CFG"
                return
            fi

            # Check config hash before and after editing
            PRE_HASH=$($MD5 "$HOMESTEAD_CFG")
            $EDITOR "$HOMESTEAD_CFG"
            POST_HASH=$($MD5 "$HOMESTEAD_CFG")

            # Run provision script if config has changed
            if [ "$PRE_HASH" != "$POST_HASH" ]; then
                echo 'Config file has changed, running provision scripts...'
                echo
                CMD='provision'
            else
                echo 'Config file has not changed.'
                return
            fi
            ;;

        # Edit the hosts file
        "host" | "hosts")
            sudo nano /etc/hosts
            return
            ;;

        # Display extended help
        "help")
            echo 'Homestead helper commands:'
            echo '     cfg            edit homestead yaml file'
            echo '     config         alias of cfg'
            echo
            CMD=help
            ;;

        # Pass command to vagrant
        *)
            CMD="$*"
            ;;

    esac


    # Run command
    (cd ~/Homestead && vagrant $CMD)

    
    # Error handling
    if [[ $? -ne 0 ]]; then
    	case "$CMD" in
    		'ssh')
				echo 'Oops, homestead is not running. Let me fix that for you...'
				hs up
				if [[ $? -eq 0 ]]; then
					hs ssh
				else
					echo "Hmm, looks like there is a problem with your configuration somewhere."
					echo "Unfortunately, I'm not smart enough to fix it :("
				fi
				;;
		esac
	fi

)}
