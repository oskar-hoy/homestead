# README #

Adds the hs shortcut, allowing you to access your homestead
box from anywhere on your system. You will also get some
extended functionality in addition to the usual vagrant commands. 

### Setup ###

* Clone repository to your ZSH custom plugins folder (Usually ~/.oh-my-zsh/custom/plugins)
* Add the plugin to your activated plugins list in .zshrc
* Restart your terminal, or launch a new shell

### Configuration ###
If your homestead.yaml is not located at ~/Homestead/homestead.yaml add the following
line to your .zshrc:

```shell
HOMESTEAD_CFG=<PATH-TO-HOMESTEAD>/homestead.yaml
```

This plugin uses the EDITOR variable to determine which editor to launch when editing
the homestead configuration. If it is not set, nano will be used by default.

### Usage ###

The usual vagrant commands works, like this:

```shell
hs up
hs provision
hs ssh
```

In addition, the following commands has been added:

```shell
# Edit the Homestead.yaml file.
# If the file changes, your homestead box will be
# reprovisioned automatically.

hs config
hs cfg
```

```shell
# Edit your /etc/hosts file
# Requires you to elevate your permissions.

hs host
hs hosts
```

If no command is given, ssh is assumed.
